// Write a function that takes two arrays (a and b) as arguments
// Create an object that has properties with keys 'a' and corresponding values 'b'
// Return the object

function myFunction(a, b) {
  // Your code here...
  let obj = {};
  a.forEach((prop, index)=>{
      obj[prop] = b[index]
  })
  return obj;
}

console.log(myFunction(['a', 'b', 'c'], [1, 2, 3]));

myFunction(['a', 'b', 'c'], [1, 2, 3]);
// Expected {a:1,b:2,c:3}
myFunction(['w', 'x', 'y', 'z'], [10, 9, 5, 2]);
// Expected {w:10,x:9,y:5,z:2}
myFunction([1, 'b'], ['a', 2]);
// Expected {1:'a',b:2}
