// Write two ways of creating objects using functions that represent
// the items of the products on the products.jpg on the source directory

// Factory Function
function createProductObj(url, title, info) {
  return {
    url,
    title,
    info,
  };
}
// qiymatlarni ixtiyoriy kiritdim

let obj1 = createProductObj(
  "https://something",
  "Cola",
  "something is a good thing"
);
let obj2 = createProductObj(
  "https://something2",
  "Cola2",
  "something2 is a good thing"
);

console.log(obj1);
console.log(obj2);



// Constructor Function
function Product(url, title, info) {
  this.url = url;
  this.title = title;
  this.info = info;
}
let obj3 = new Product(
  "https://something",
  "Cola",
  "something is a good thing"
);
let obj4 = new Product(
  "https://something2",
  "Cola2",
  "something2 is a good thing"
);

console.log(obj3);
console.log(obj4);
